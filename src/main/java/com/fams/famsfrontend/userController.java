package com.fams.famsfrontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class userController {

    @GetMapping
    public String index(){
        return "user";
    }
}
